import cv2
import numpy as np
import argparse

ruta = 'med.png'
image = cv2.imread(ruta)
gris = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# apply the 3x3 mean filter on the image
kernel = np.ones((3,3),np.float32)/9
processed_image = cv2.filter2D(gris,-1,kernel)
# cv2.imwrite('processed_image2.png', processed_image)
gaussiana = cv2.GaussianBlur(processed_image, (5,5), 0)
canny = cv2.Canny(gaussiana, 110, 120)
cv2.imwrite('canny.png', canny)
