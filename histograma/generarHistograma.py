import matplotlib.pyplot as pl
from os import scandir, getcwd

altos = []
anchos = []
areas = []

def generarFigura(array, nombre):
    # pl.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    
    pl.hist(array, bins = 'auto')#, len(array), (min(array),max(array)))
    pl.title(nombre)
    pl.xlabel("Valor en px.")
    pl.ylabel("Frecuencia")
    pl.savefig(nombre + ".png", dpi=250)
    pl.close()

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

def encontrarMinimosMaximos(linea):
    array = []
    while(True):
        try:
            posXmin = linea.index('Xmin') + 6
            linea = linea[posXmin:]
            coma = linea.index(',')
            xMin= linea[:coma]
            
            
            posYmin = linea.index('Ymin') + 6
            linea = linea[posYmin:]
            coma = linea.index(',')
            yMin= linea[:coma]
            
            
            posXmax = linea.index('Xmax') +6
            linea = linea[posXmax:]
            coma = linea.index(',')
            xMax= linea[:coma]
            
            
            posYmax = linea.index('Ymax') + 6
            linea = linea[posYmax:]
            coma = linea.index('}')
            yMax= linea[:coma]
            
            ''' print('Xmin', xMin)
            print('Xmax', xMax)
            print('Ymin', yMin)
            print('Ymax', yMax)'''
            
            ancho = int(xMax) - int(xMin)
            alto = int(yMax) - int(yMin)
            
            altos.append(alto)
            anchos.append(ancho)
            areas.append(alto * ancho)

            array.append(ancho)
            array.append(alto)
        except:
            break
    return array


ruta = 'D:\\test\\BARCOS_P4 CLAUDIA\\Annotations\\'
arreglo = ls(ruta)
contadorBarcos = 0
for i in arreglo:
    # print(i, ': ', end="")
    archivo = open(ruta + i, "r")
    for linea in archivo.readlines():
        a = encontrarMinimosMaximos(linea)
        contadorBarcos += len(a)/2
        # print(len(a)/2)
        archivo.close()

print('Barcos en total: ', contadorBarcos)

generarFigura(altos, "Altos")
generarFigura(anchos, "Anchos")
generarFigura(areas, "areas")