from os import scandir, getcwd
import shutil

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

def mover(origen, destino):
    # ruta = os.getcwd() + os.sep
    with open(origen, 'rb') as forigen:
        with open(destino, 'wb') as fdestino:
            shutil.copyfileobj(forigen, fdestino)
            # print("Archivo copiado")

def nombre(nomOri):
    punto = nomOri.index('.')
    return nomOri[0:punto] + '.jpg'
    
def contar(archivo):
    contLocal = 0
    for linea in archivo.readlines():
        if (buscar in linea):
            contLocal = linea.count(buscar)
    return contLocal

ruta = '/home/aaron/Documentos/Datasets/AIRBUS/Annotations/'
rutaImgs = '/home/aaron/AIRBUS/train_v2/'
destino = '/home/aaron/AIRBUS/barcos/'
arreglo = ls(ruta)
cont = 0
buscar = "object"
contIns = 0

imagenesConBarcos = 0
imagenesSinBarcos = 0

for i in arreglo:
    if (cont % 20000 == 0 and cont != 0):
        print(cont , "de", len(arreglo))

    archivo = open(ruta + i, "r")
    bi = contar(archivo)

    if (bi == 0):
        imagenesSinBarcos = imagenesSinBarcos + 1
    else:
        imagenesConBarcos = imagenesConBarcos + 1
    contIns = contIns + bi

            # nameImg = nombre(i)
            # print(rutaImgs + nameImg)
            # mover(rutaImgs + nameImg, destino + nameImg)
            # break
    archivo.close()   
    cont = cont + 1
    
print("Imagenes: ", len(arreglo))
print("Imagenes con barcos: ", imagenesConBarcos)
print("Imagenes sin barcos: ", imagenesSinBarcos)
print("Total", imagenesConBarcos + imagenesSinBarcos)
print("Objetos|: ", contIns)
