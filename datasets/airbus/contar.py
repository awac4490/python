from os import scandir, getcwd
from PIL import Image
import matplotlib.pyplot as pl
from statistics import mode


def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

ruta = input('ruta: ')
# ruta = 'D:\Satelitales\images'
arreglo = ls(ruta)
tamanos = []
histo = []
contTam = []

tamX = []
tamY = []

porcent = 0
cont = 0
for i in arreglo:
    # print(cont, next)
    next = int(porcent * len(arreglo) / 100)
    if (cont == next):
        print(cont, "de", len(arreglo), "->", porcent, "%")
        porcent += 10
    imagen = Image.open(ruta + i)
    size = str(imagen.size)
    histo.append(imagen.size)
    if (size in tamanos):
        pos = tamanos.index(size)
        contTam[pos] = contTam[pos] + 1
    else:
        tamanos.append(size)
        contTam.append(1)
    cont += 1
    
print("Imagenes: ", len(arreglo))

''' zipped = zip(tamanos, contTam)
for i, j in zipped:
    print(i,":\t",  j)'''

tamMayor = max(contTam)
pos = contTam.index(tamMayor)

print(tamMayor, tamanos[pos])

pl.bar(tamanos, contTam) # A bar chart
pl.xlabel('Tamaños')
pl.ylabel('Frecuencia')
pl.xticks(rotation='vertical')
pl.show()

print(mode(histo))

pl.hist(histo)#, bins = 'auto')
# plt.hist(mos, bins = 60)
pl.legend()
# 
pl.show()

pl.hist(histo, bins = 'auto')
pl.show()