from os import scandir, getcwd
import shutil

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]
    
def contar(archivo):
    contLocal = 0
    for linea in archivo.readlines():
        if (buscar in linea):
            ## print('--------------------', linea)
            contLocal = contLocal + 1
    return contLocal

ruta = '/home/aaron/Documentos/Datasets/Masati/MASATI-v2/All labels/'
arreglo = ls(ruta)
cont = 0
buscar = "ship"
contIns = 0

imagenesConBarcos = 0
imagenesSinBarcos = 0

for i in arreglo:
    if (cont % 1000 == 0 and cont != 0):
        print(cont , "de", len(arreglo))

    archivo = open(ruta + i, "r")
    bi = contar(archivo)

    if (bi == 0):
        imagenesSinBarcos = imagenesSinBarcos + 1
    else:
        imagenesConBarcos = imagenesConBarcos + 1
    contIns = contIns + bi

            # nameImg = nombre(i)
            # print(rutaImgs + nameImg)
            # mover(rutaImgs + nameImg, destino + nameImg)
            # break
    archivo.close()   
    cont = cont + 1
    
print("Imagenes: ", len(arreglo))
print("Imagenes con barcos: ", imagenesConBarcos)
print("Imagenes sin barcos: ", imagenesSinBarcos)
print("Total", imagenesConBarcos + imagenesSinBarcos)
print("Instancias: ", contIns)
