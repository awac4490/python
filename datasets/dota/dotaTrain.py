from os import scandir, getcwd
import shutil
import numpy as np

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

def mover(origen, destino):
    # ruta = os.getcwd() + os.sep
    with open(origen, 'rb') as forigen:
        with open(destino, 'wb') as fdestino:
            shutil.copyfileobj(forigen, fdestino)
            # print("Archivo copiado")

def nombre(nomOri):
    punto = nomOri.index('.')
    return nomOri[0:punto] + '.jpg'
    
def contar(archivo):
    # contLocal = 0
    for linea in archivo.readlines():
        x = linea.split()
        if (len(x) == 10):
            obj = x[len(x)-2]
            if (not obj in arrObjects):
                arrObjects.append(obj)
                conObjects.append(0)
            else:
                pos = arrObjects.index(obj)
                conObjects[pos] = conObjects[pos] + 1
        # if (buscar in linea):
            # contLocal = linea.count(buscar)
    return 0

ruta = '/home/aaron/Documentos/Datasets/Dota/1. train/labelTxt-v1.5/DOTA-v1.5_train_hbb/'
# rutaImgs = '/home/aaron/AIRBUS/train_v2/'
# destino = '/home/aaron/AIRBUS/barcos/'
arreglo = ls(ruta)
cont = 0
buscar = "SHIP"
contIns = 0
arrObjects = []
conObjects = []
imagenesConBarcos = 0
imagenesSinBarcos = 0

for i in arreglo:
    if (cont % 20000 == 0 and cont != 0):
        print(cont , "de", len(arreglo))

    archivo = open(ruta + i, "r")
    bi = contar(archivo)

    if (bi == 0):
        imagenesSinBarcos = imagenesSinBarcos + 1
    else:
        imagenesConBarcos = imagenesConBarcos + 1
    contIns = contIns + bi

            # nameImg = nombre(i)
            # print(rutaImgs + nameImg)
            # mover(rutaImgs + nameImg, destino + nameImg)
            # break
    archivo.close()   
    cont = cont + 1

print("DATASET DOTA - \"train\"")
print("Imagenes: ", len(arreglo))

totalObjetos = np.sum(conObjects)
print('TOTAL: ', totalObjetos)
barcosss = 0

zipped = zip(arrObjects, conObjects)
for i, j in zipped:
    print(i,":\t",  j)
    if (i == 'ship'):
        barcosss = j

print('Objetos sin barcos: ', totalObjetos - barcosss)