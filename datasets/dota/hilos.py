# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 15:29:49 2019

@author: aaron
"""
import threading
from os import scandir, getcwd

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

contIns = 0
def contar(nombre):
    # print(nombre)
    contLocal = 0
    archivo = open(ruta + i, "r")
    for linea in archivo.readlines():
        if (buscar in linea):
            contLocal = contLocal + 1
    contIns = contIns + contLocal
    return

def worker(count):
    """funcion que realiza el trabajo en el thread"""
    print("Archivo %s" % count)
    return

ruta = '/home/aaron/AIRBUS/Annotations/'
buscar = "SHIP"
threads = list()

arreglo = ls(ruta)

for i in arreglo:
    t = threading.Thread(target=contar, args=(i,))
    threads.append(t)
    t.start()

# [t.join() for t in threads]

print(contIns)
print("Total: ", len(threads))
