from os import scandir, getcwd
import shutil
import json

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

def mover(origen, destino):
    # ruta = os.getcwd() + os.sep
    with open(origen, 'rb') as forigen:
        with open(destino, 'wb') as fdestino:
            shutil.copyfileobj(forigen, fdestino)
            # print("Archivo copiado")

def nombre(nomOri):
    punto = nomOri.index('.')
    return nomOri[0:punto] + '.jpg'
    
def contar(archivo):
    # contLocal = 0
    for linea in archivo.readlines():
        x = linea.split()
        if (len(x) == 10):
            obj = x[len(x)-2]
            if (not obj in arrObjects):
                arrObjects.append(obj)
                conObjects.append(0)
            else:
                pos = arrObjects.index(obj)
                conObjects[pos] = conObjects[pos] + 1
        # if (buscar in linea):
            # contLocal = linea.count(buscar)
    return 0

ruta = '/home/aaron/Documentos/Datasets/Kaggle SF/shipsnet.json'
# rutaImgs = '/home/aaron/AIRBUS/train_v2/'
# destino = '/home/aaron/AIRBUS/barcos/'
## arreglo = ls(ruta)
cont = 0
buscar = "SHIP"
contIns = 0
arrObjects = []
conObjects = []
imagenesConBarcos = 0
imagenesSinBarcos = 0


#prompt the user for a file to import
filter = "JSON file (*.json)|*.json|All Files (*.*)|*.*||"

print("DATASET KAGGLE SAN FRANCISCO")
with open(ruta) as json_file:
    archivo = json.load(json_file)
    cadena = archivo['labels']
    print("Imagenes sin barcos: ", cadena.count(0))
    print("Imagenes con barcos: ", cadena.count(1))