from os import scandir, getcwd
import shutil
from PIL import Image
import matplotlib.pyplot as pl

def generarFigura(array, nombre):
    # pl.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    
    pl.hist(array, bins = 'auto')#, len(array), (min(array),max(array)))
    pl.title(nombre)
    pl.xlabel("Valor en px.")
    pl.ylabel("Frecuencia")
    pl.savefig(nombre + ".png", dpi=250)
    pl.close()

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

ruta = '/home/aaron/Documentos/Datasets/Kaggle SF/shipsnet/'
arreglo = ls(ruta)
anchos = []
altos = []
tamanos = []
contTam = []

for i in arreglo:
    imagen = Image.open(ruta + i)
    ancho, alto = imagen.size
    anchos.append(ancho)
    altos.append(alto)
    if (imagen.size in tamanos):
        pos = tamanos.index(imagen.size)
        contTam[pos] = contTam[pos] + 1
    else:
        tamanos.append(imagen.size)
        contTam.append(1)
    
print("Imagenes: ", len(arreglo))
print(tamanos)
print(contTam)
# generarFigura(tamanos, "Altos")
