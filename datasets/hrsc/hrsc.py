from os import scandir, getcwd
import shutil

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

def mover(origen, destino):
    # ruta = os.getcwd() + os.sep
    with open(origen, 'rb') as forigen:
        with open(destino, 'wb') as fdestino:
            shutil.copyfileobj(forigen, fdestino)
            # print("Archivo copiado")

def nombre(nomOri):
    punto = nomOri.index('.')
    return nomOri[0:punto] + '.jpg'
    
def contar(archivo):
    contLocal = 0
    for linea in archivo.readlines():
        if (buscar in linea):
            initClass = linea.index('>') + 1
            endClass = linea.index('</')
            clase = linea[initClass:endClass]
            if (clase in clases):
                pos = clases.index(clase)
                contClases[pos] = contClases[pos] + 1
            else:
                clases.append(clase)
                contClases.append(1)
            contLocal += 1
    return contLocal

ruta = '/home/aaron/Documentos/Datasets/HRSC/HRSC2016/FullDataSet/Annotations/'
arreglo = ls(ruta)
cont = 0
buscar = "Class_ID"
contIns = 0

clases = []
contClases = []

imagenesConBarcos = 0
imagenesSinBarcos = 0

for i in arreglo:
    if (cont % 20000 == 0 and cont != 0):
        print(cont , "de", len(arreglo))

    archivo = open(ruta + i, "r")
    bi = contar(archivo)

    if (bi == 0):
        imagenesSinBarcos = imagenesSinBarcos + 1
    else:
        imagenesConBarcos = imagenesConBarcos + 1
    contIns = contIns + bi

            # nameImg = nombre(i)
            # print(rutaImgs + nameImg)
            # mover(rutaImgs + nameImg, destino + nameImg)
            # break
    archivo.close()   
    cont = cont + 1

print("Imagenes con objetos: ", imagenesConBarcos)
print("Imagenes sin objetos: ", imagenesSinBarcos)
print("Total", imagenesConBarcos + imagenesSinBarcos)
print("Objetos: ", contIns)

zipped = zip(clases, contClases)
for i, j in zipped:
    print(i,":\t",  j)