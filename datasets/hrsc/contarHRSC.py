from os import scandir, getcwd
from PIL import Image
import matplotlib.pyplot as pl
from statistics import mode
import numpy as np
from collections import Counter

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

ruta = input('ruta: ')
# ruta = 'D:\\Satelitales\\images\\'
arreglo = ls(ruta)
tamanos = []

tamX = []
tamY = []

porcent = 0
cont = 0
print("Imagenes: ", len(arreglo))
for i in arreglo:
    # print(cont, next)
    next = int(porcent * len(arreglo) / 100)
    if (cont == next):
        print(cont, '... ', end='')
        porcent += 10
    imagen = Image.open(ruta + i)
    size = imagen.size
    tamX.append(size[0])
    tamY.append(size[1])
    cont += 1

print(len(arreglo))

c = Counter(tamX)
print('MODA X', c.most_common(2))
c = Counter(tamY)
print('MODA Y', c.most_common(2))

print('PROMEDIO X:', np.mean(tamX))
print('PROMEDIO Y:', np.mean(tamY))

pl.hist(tamX, bins = 'auto')
pl.show()

pl.hist(tamY, bins = 'auto')
pl.show()